import React, { useEffect } from 'react';
import { SafeAreaView, StyleSheet, Text, StatusBar, Button, ToastAndroid, PermissionsAndroid, Platform } from 'react-native';

import { DeviceEventEmitter } from 'react-native';
import ReactNativeForegroundService from '@supersami/rn-foreground-service';
// importing Service to call
import RNLocation from 'react-native-location';

const App = () => {
  // NOTIFICATION PARAMETERS : these are the parameters we have to start a foreground service , * indicate Required.
  // id, * // Id of the notification must be a int
  // title // title of the notification
  // message  // message inside of the notification
  // vibration
  // visibility  ,
  // icon // if you want to use custom icons, you can just use theme by giving their name without extension, make sure they are drawable. for eg = ic_launcher,
  // largeicon // if you want to use custom icons, you can just use theme by giving their name without extension, make sure they are drawable. for eg = ic_launcher,
  // importance // importance, you can read about it on google's official notification docs
  // number // importance, you can read about it on google's official notification docs
  // button // want a button in the notification, set it true
  // buttonText // the text of the button.
  // buttonOnPress // onPress Button, set a string here, onClick your app will reopen with that string in device emitter
  // mainOnPress  // onPress Main Notification, set a string here, onClick your app will reopen with that string in device emitter

  useEffect(() => {
    // device event emitter used to
    let subscription = DeviceEventEmitter.addListener(
      'notificationClickHandle',
      function (e) {
        console.log('json', e);
      },
    );
    return function cleanup() {
      subscription.remove();
    };
  }, []);
  let locationSubscription = null;
  let locationTimeout = null;

  const requestPer = () => {
    PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_BACKGROUND_LOCATION,
      {
        title: 'Background Location Permission',
        message:
          'We need access to your location ' +
          'so you can get live quality updates.',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
  }

  const getLocation = () => {
    RNLocation.requestPermission({
      ios: 'whenInUse',
      android: {
        detail: 'fine',
      },
    }).then((granted) => {
      console.log('Location Permissions: ', granted);
      // if has permissions try to obtain location with RN location
      if (granted) {
        locationSubscription && locationSubscription();
        locationSubscription = RNLocation.subscribeToLocationUpdates(
          ([locations]) => {
            Platform.OS === 'android' && locationSubscription();
            locationTimeout && clearTimeout(locationTimeout);
            fetch('http://10.10.20.178:3000', {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              body: JSON.stringify(locations)
            });
            Platform.OS === 'android' && ToastAndroid.showWithGravity(
              JSON.stringify(locations),
              ToastAndroid.SHORT,
              ToastAndroid.CENTER
            );
          },
        );
      } else {
        locationSubscription && locationSubscription();
        locationTimeout && clearTimeout(locationTimeout);
        console.log('no permissions to obtain location');
      }
    });
  }


  const onStart = () => {

    if (Platform.OS === 'ios') {
      getLocation()
      return
    } else {
      requestPer()
    }



    RNLocation.configure({
      distanceFilter: 100, // Meters
      desiredAccuracy: {
        ios: 'best',
        android: 'balancedPowerAccuracy',
      },
      // Android only
      androidProvider: 'auto',
      interval: 1000, // Milliseconds
      fastestInterval: 10000, // Milliseconds
      maxWaitTime: 5000, // Milliseconds
      // iOS Only
      activityType: 'other',
      allowsBackgroundLocationUpdates: true,
      headingFilter: 1, // Degrees
      headingOrientation: 'portrait',
      pausesLocationUpdatesAutomatically: true,
      showsBackgroundLocationIndicator: true,
    });


    // Checking if the task i am going to create already exist and running, which means that the foreground is also running.
    if (ReactNativeForegroundService.is_task_running('taskid')) return;

    ReactNativeForegroundService.add_task(
      getLocation,
      {
        delay: 1000,
        onLoop: true,
        taskId: 'taskid',
        onError: (e) => console.log('Error logging:', e),
      },
    );


    // Creating a task.
    // ReactNativeForegroundService.add_task(
    //   () => {
    //     setInterval(() => {
    //       ToastAndroid.showWithGravity(
    //         "All Your Base Are Belong To Us",
    //         ToastAndroid.SHORT,
    //         ToastAndroid.CENTER
    //       );
    //     }, 3000);
    //   },
    //   {
    //     delay: 3000,
    //     onLoop: false,
    //     taskId: 'taskid',
    //     onError: (e) => console.log(`Error logging:`, e),
    //   },
    // );
    // starting  foreground service.
    return ReactNativeForegroundService.start({
      id: 144,
      title: 'Foreground Service',
      message: 'you are online!',
    });
  };

  const onStop = () => {
    // Make always sure to remove the task before stoping the service. and instead of re-adding the task you can always update the task.
    if (ReactNativeForegroundService.is_task_running('taskid')) {
      ReactNativeForegroundService.remove_task('taskid');
    }
    // Stoping Foreground service.
    return ReactNativeForegroundService.stop();
  };
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView
        style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>An Example for React Native Foreground Service. </Text>
        <Button title={'Start'} onPress={onStart} />
        <Button title={'Stop'} onPress={onStop} />
      </SafeAreaView>
    </>
  );
};

export default App;