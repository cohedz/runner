import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import MapboxGL from "@react-native-mapbox-gl/maps";

MapboxGL.setAccessToken("pk.eyJ1IjoiY29oZWR6IiwiYSI6ImNrbG0wdGk5ZDA0aTYydm1mcmJoODQ2a3gifQ.7YS4pqVkM-EEw1cwif5bnA");

const styles = StyleSheet.create({
  page: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  container: {
    height: 300,
    width: 300,
    backgroundColor: "tomato"
  },
  map: {
    flex: 1
  }
});

export default class Map extends Component {
  componentDidMount() {
    MapboxGL.setTelemetryEnabled(false);
  }

  render() {
    return (
      // <View style={styles.page}>
      //   <View style={styles.container}>
      <MapboxGL.MapView style={styles.map} />
      //   </View>
      // </View>
    );
  }
}