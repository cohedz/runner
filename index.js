/**
 * @format
 */

import { AppRegistry, Platform } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import ReactNativeForegroundService from "@supersami/rn-foreground-service";
import Map from './Map';
import BackgroundLocation from './BackgroundLocation';
ReactNativeForegroundService.register();
AppRegistry.registerComponent(appName, () => Platform.select({
    android: App,
    ios: BackgroundLocation
}));
